﻿#include <iostream>
#include <string>

class Lesson17 {
private:
    std::string agentCode;
    std::string greeting;
    std::string defaultGreetPhrase = "Welcome on board, ";

    std::string SetNewAgent(std::string newAgentCode) {
        return "Agent <" + newAgentCode + ">";
    }

public:
    Lesson17() {
        greeting = "Your're not an agent! Intruder alert!!!";
    }

    Lesson17(std::string defAgentCode) {
        agentCode = SetNewAgent(defAgentCode);
        greeting = defaultGreetPhrase + agentCode;
    }

    void GetGreeting() {
        std::cout << greeting << std::endl;
    }

    void SetGreeting(std::string str) {
        if (agentCode.empty()) {
            std::cout << "ha-ha, Mr.Intruder! You are not able to do this!" << std::endl;
            return;
        }

        greeting = str + agentCode;
    }

    std::string GetAgentCode() {
        if (agentCode.empty()) {
            return "Mr.Intruder";
        }

        return agentCode;
    }

    void SetAgentCode(std::string newCode) {
        agentCode = SetNewAgent(newCode);
        SetGreeting(defaultGreetPhrase);
    }

};
class Vector {
private:
    double x, y, z;
public:
    Vector() : x(0), y(0), z(0)
    {}

    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}

    void Show() {
        std::cout << '\n' << x << ' ' << y << ' ' << z << std::endl;
    }

    void Module() {
        std::cout << "Vector (x:" << x << " y:" << y << " z:" << z << ") module: " << sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2)) << std::endl;
    }
};

int main() {
    Lesson17 agent;

    agent.GetGreeting();
    agent.SetGreeting("Welcome Mr.President");

    std::cout << agent.GetAgentCode() << std::endl;

    agent.SetAgentCode("007");

    agent.GetGreeting();

    Vector v(3, 4, 11);
    v.Module();

    return 0;
}